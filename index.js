// Packages used
const express = require("express");
const app = express();
const readline = require("readline");
const fs = require('fs');
const os = require("os");

// Port to run server
const port = 3000;

// route "/" to return hello world
app.get("/", (req, res) => {
    res.send("Hello world!");
});



// Interface for input / ouptut
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


// String to hold the user prompt
const userPrompt = `
    Choose an option:
    1. Read package.json 
    2. Display OS info
    3. Start HTTP server
    Type a number: `;

// Reads package.json and outputs it
function readPackageJson() {
    let jsonDataRaw = fs.readFileSync("package.json");
    let jsonData = JSON.parse(jsonDataRaw);
    console.log(jsonData);
}

// Displays os info
function displayOsInfo() {
    let osInfo = `
        architecture: ${os.arch()}
        CPU cores: ${os.cpus().length}
        Total memory: ${os.totalmem()}
        Free memory: ${os.freemem()}
        Hostname: ${os.hostname()}
        Platform: ${os.platform()}
        User: ${os.userInfo().username}
        Version: ${os.version()}
    `;
    console.log(osInfo);
}

// Starts the server listening on port 3000
function startServer() {
    app.listen(port, () => {
        console.log(`Server running on http://localhost:${port}/`);
    });
}

// Prompt user for input
rl.question(userPrompt, (answer) => {
    if (answer.trim() == 1) {
        readPackageJson();
    } else if (answer.trim() == 2) {
        displayOsInfo();
    } else if (answer.trim() == 3) {
        startServer();
    } else {
        console.log(`invalid input: ${answer}`);
    }
    rl.close();
});